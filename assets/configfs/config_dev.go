// +build dev

package configfs

import "net/http"

// Assets includes postgres/init.sql
var Assets http.FileSystem = http.Dir("./files")
