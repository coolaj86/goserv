package api

import (
	"encoding/base64"
	"time"

	"git.example.com/example/goserv/internal/db"
)

func newID() string {
	// Postgres returns IDs on inserts but,
	// for portability and ease of association,
	// we'll create our own.
	b := make([]byte, 16)
	_, _ = RandReader.Read(b)
	id := base64.RawURLEncoding.EncodeToString(b)
	return id
}

// NotDeleted supplements a WHERE clause
const NotDeleted = `
 ( "deleted_at" IS NULL OR "deleted_at" = '0001-01-01 00:00:00+00' OR "deleted_at" = '1970-01-01 00:00:00+00' ) 
`

func logEvent(action, table, recordID, by string, at time.Time) (string, error) {
	id := newID()

	if _, err := db.DB.Exec(`
		INSERT INTO "events" ("id", "action", "table", "record", "by", "at")
		VALUES ($1, $2, $3, $4, $5, $6)`,
		id, action, table, recordID, by, at,
	); nil != err {
		return "", err
	}

	return id, nil
}
