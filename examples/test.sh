#!/bin/bash

export BASE_URL=http://localhost:7070
#export BASE_URL=https://example.com
#CURL_OPTS="-sS"
CURL_OPTS=""

mkdir -p ./tmp/

# Sign an Admin token
echo '{ "sub": "admin_ppid", "email": "me@example.com", "iss": "'"${BASE_URL}"'" }' > ./tmp/admin.claims.json
keypairs sign --exp 1h ./key.jwk.json ./tmp/admin.claims.json > ./tmp/admin.jwt.txt 2> ./tmp/admin.jws.json
export ADMIN_TOKEN=$(cat ./tmp/admin.jwt.txt)

# verify the Admin token
#keypairs verify ./pub.jwk.json ./admin.jwt.txt


# Sign a User token
echo '{ "sub": "random_ppid", "email": "me@example.com", "iss": "'"${BASE_URL}"'" }' > ./tmp/user.claims.json
keypairs sign --exp 1h ./key.jwk.json ./tmp/user.claims.json > ./tmp/user.jwt.txt 2> ./tmp/user.jws.json
export USER_TOKEN=$(cat ./tmp/user.jwt.txt)

# verify the User token
#keypairs verify ./pub.jwk.json ./user.jwt.txt


EID=$(cat ./user.jws.json | grep sub | cut -d'"' -f 4)

echo ""
echo 'DELETE /api/public/reset (only works in --demo mode, deletes all data)'
curl $CURL_OPTS -X DELETE "${BASE_URL}/api/public/reset"
echo ""

echo ""
echo "Bootstrap with a new admin (only works once)"
curl -f $CURL_OPTS -X POST "${BASE_URL}/api/public/setup" \
    -H "Authorization: Bearer ${ADMIN_TOKEN}"
echo ""

echo "Create a new user"
curl $CURL_OPTS -X POST "${BASE_URL}/api/users" \
    -H "Authorization: Bearer ${USER_TOKEN}" \
    -d '{ "display_name": "Jo Doe" }'
echo ""
