(function () {
  "use strict";

  // AJQuery
  function $(sel, el) {
    if (!el) {
      el = document;
    }
    return el.querySelector(sel);
  }
  function $$(sel, el) {
    if (!el) {
      el = document;
    }
    return el.querySelectorAll(sel);
  }

  function displayToken(token) {
    $$(".js-token").forEach(function (el) {
      el.innerText = token;
    });
  }

  Pocket.onToken(function (token) {
    // TODO Pocket v1.0 will make this obsolete
    localStorage.setItem("pocket-token", token);
    displayToken();
  });
  displayToken(localStorage.getItem("pocket-token"));

  // requires div with class 'pocket'
  $("form.js-signin").addEventListener("submit", function (ev) {
    ev.preventDefault();
    ev.stopPropagation();

    var email = $("[name=email]").value;
    Pocket.openSignin(ev, { email: email });
  });

  $("form.js-healthcheck").addEventListener("submit", function (ev) {
    ev.preventDefault();
    ev.stopPropagation();

    window.fetch("/api/public/ping").then(async function (resp) {
      var res = await resp.json();
      $(".js-server-health").innerText = JSON.stringify(res, null, 2);
    });
  });
  `
    # Demo Mode Only
    DELETE /public/reset                                    Drop database and re-initialize

    # Public
    GET  /public/ping                                       Health Check
    POST /public/setup                  <= (none)           Bootstrap

    # Admin-only
    GET  /admin/ping                                        (authenticated) Health Check
    GET  /admin/users                      []Users =>       List ALL Users

    # User
    GET  /user/ping                                         (authenticated) Health Check
    GET  /user                          => User             User profile
    `
    .trim()
    .split(/\n/)
    .forEach(function (line) {
      line = line.trim();
      if ("#" === line[0] || !line.trim()) {
        return;
      }
      line = line.replace(/(<=)?\s*\(none\)\s*(=>)?/g, "");
      var parts = line.split(/\s+/g);
      var method = parts[0];
      if ("GET" != method) {
        method = "-X " + method + " ";
      } else {
        method = "";
      }
      var pathname = parts[1];
      var auth = pathname.match(/(public|user|admin)/)[1];
      if ("admin" == auth) {
        auth = " \\\n    -H 'Authorization: Bearer ADMIN_TOKEN'";
      } else if ("user" == auth) {
        auth = " \\\n    -H 'Authorization: Bearer USER_TOKEN'";
      } else {
        auth = "";
      }
      document.body.querySelector(".js-pre").innerHTML += (
        `
                <div class="card-text">
                    <pre><code>curl -X POST https://example.com/api</code></pre>
                    <pre><code class="js-` +
        pathname.replace(/\//g, "-") +
        `">-</code></pre>
                </div>
            `
      )
        .replace(
          /https:\/\/example\.com/g,
          location.protocol + "//" + location.host
        )
        .replace(/-X POST /g, method)
        .replace(/\/api/g, "/api" + pathname + auth);
    });
  /*
    document.body.querySelector(".js-pre").innerHTML = document.body
        .querySelector(".js-pre")
        .innerHTML
        */
})();
