module git.example.com/example/goserv

go 1.15

require (
	git.rootprojects.org/root/go-gitver/v2 v2.0.2
	git.rootprojects.org/root/keypairs v0.6.3
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.8.0
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20200824052919-0d455de96546
	golang.org/x/tools v0.0.0-20201001230009-b5b87423c93b
	google.golang.org/appengine v1.6.6 // indirect
)
